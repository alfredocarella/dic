/* Trigger pulse sender. It also sends analog reads via USB using the Firmata protocol. */


#include <Firmata.h>


// Definition of program constants:
const int OUTPUT_PIN =  13;           // the number of the LED pin
const float THRESHOLD_VOLTAGE = 1.3;  // Threshold value in volts
const int TRIGGER_DURATION = 12;      // Trigger flank duration in ms. Must be at least 10 for the StrainMaster
const int LOCK_DURATION = 988;        // Must be at least 1000ms/(max frame rate)


// Initialization of program variables:
int triggerState = LOW;             // triggerState is used to set the output value
unsigned long triggerDown = 0;      // scheduled time for the trigger to turn off
unsigned long triggerUnlock = 0;    // time for the system to start accepting a new trigger
float voltage = 0;                  // converted value [0-1023] -> [0-5V]
boolean triggerAllowed = false;


// Firmata definitions
byte analogPin = 0;
void analogWriteCallback(byte pin, int value)
{
  if (IS_PIN_PWM(pin)) {
    pinMode(PIN_TO_DIGITAL(pin), OUTPUT);
    analogWrite(PIN_TO_PWM(pin), value);
  }
}



void setup() 
{
  Firmata.setFirmwareVersion(FIRMATA_MAJOR_VERSION, FIRMATA_MINOR_VERSION);
  Firmata.attach(ANALOG_MESSAGE, analogWriteCallback);
  Firmata.begin(57600);
  
  // set the digital pin as output:
  pinMode(OUTPUT_PIN, OUTPUT);
}


void loop() 
{  
  // Run trigger logic
  unsigned long currentTime = millis();
  voltage = analogRead(A0) * (5.0 / 1023.0);      
  if (currentTime < triggerDown) {
    digitalWrite(OUTPUT_PIN, HIGH);
  } else {
    if (currentTime < triggerUnlock) {
      digitalWrite(OUTPUT_PIN, LOW);
    } else {
      if (voltage < THRESHOLD_VOLTAGE) {
        digitalWrite(OUTPUT_PIN, LOW);
        triggerAllowed = true;
      } else {
        if (triggerAllowed) {
          triggerDown = currentTime + TRIGGER_DURATION;
          triggerUnlock = triggerDown + LOCK_DURATION;
          triggerAllowed = false;
          digitalWrite(OUTPUT_PIN, HIGH);          
        }
      }
    }
  }

  // Run Firmata communication loop
  while (Firmata.available()) {
    Firmata.processInput();
  }
  // do one analogRead per loop to avoid delay in analog writes
  Firmata.sendAnalog(analogPin, analogRead(analogPin));
  analogPin = analogPin + 1;
  if (analogPin >= TOTAL_ANALOG_PINS) analogPin = 0;
}

