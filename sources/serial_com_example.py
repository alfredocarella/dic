import pyfirmata

# Remember to use an Arduino sketch that initializes the Firmata protocol
print "Initializing Arduino board"
port = "COM5"  # "/dev/ttyACM0"  # Serial port names vary according to the OS
board = pyfirmata.Arduino(port, baudrate=57600)  # Match baudrate in Arduino sketch
it = pyfirmata.util.Iterator(board)
it.start()

# Define board inputs to be read (just shortcuts to make it easier to debug)
a0 = board.get_pin('a:0:i')
output_pin = 13  # Output pin 13 has its own built-in led. Easier for testing.


# As a simple example, read the analog input from A0 every second and turn on 
# the led in D13 if the input voltage is higher than 3V. Stop after 10 samples.
import time

voltage_threshold = 3.0
number_of_samples = 10
for sample in range(number_of_samples):
    if a0.read() > voltage_threshold:
	    board.digital[output_pin].write(True)
	else:
	    board.digital[output_pin].write(False)
    time.sleep(interval)

board.digital[output_pin].write(False)  # Turn off the led at the end
board.exit()  # Release the Arduino board
