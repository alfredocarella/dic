-----
Dear Alfredo,

Thank you for your e-mail and additional information.

I would like to start by clarifying what you have already pointed out, in that the ADC recorded input can be indeed used as a start recording flag when the imposed condition (start threshold of measured signal) is reached. In turn, this means that the system will operate at a given fixed acquisition rate set in the recording menu, as you have already mentioned. I would like to note however, that you could use a loop in the recording sequence and to record one image inside each loop, which would be triggered on a given threshold. That being said, there will be a jitter between reaching the threshold and the actual recorded image since the system is operated at a fixed acquisition rate and therefore it will depend when the ADC is recording the threshold value with respect to the camera triggers.  

As for the limitations that you have mentioned regarding the TTL trigger, I am not sure that I understand correctly what you are trying to achieve. Is it that you are trying to use a signal (e.g. a load cell output) to trigger the StrainMaster system, but at the same time you would like to know exactly what the value of the analog signal is? If so, why not use a splitter and connect the analog signal to the ADC input, and at the same time convert that same signal to a TTL trigger using a separate circuit? That will mean that you have a trigger for the system and you will record the value of the signal through the ADC (with a maximum jitter of 10 ms which should be fine considering the load will not change significantly during that short period).   

Kind regards,
Your Service-Team
Dr. Alex Nila 
-----