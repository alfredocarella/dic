-----
Dear Alfredo Carella,

Thank you for your e-mail, and for your interest in finding out more information about our system and best practices when using the StrainMaster system.

I understand from the document you have attached, that you have no problems in setting up the system and driving all the hardware (timing unit, camera, lights) using DaVis. Is that accurate?

Regarding the trigger, there are a few aspects that I would like to clarify. 
Firstly, there are two ways to trigger the StrainMaster system: one way is to use the 'Trigger in' line on the timing unit, while another option is to use the A/D converter channels. I would like to point out already that using the A/D converter is the recommended option, as it allows you to have more control over the way the system is triggered. Since your timing unit is foreseen with such an ADC (as our records show), that will probably be the best option.

If you do however want to trigger the system using the trigger in line of the timing unit, that can also be achieved as long as you use a 5V TTL (which you have mentioned that you are using). I would however like to point out that, after checking our records, I have noticed that your timing unit is a StrainMaster controller, and not a PTU X (which was mentioned in your document). Although these two devices are very similar in capabilities and have similar architecture of the timing unit itself, there are a few fundamental differences. Most important of which, is the connectors used. The PTU unit is foreseen with a large variety of connector possibilities (and each connector has multiple I/O lines), as these units are capable of driving both DIC systems as well as PIV (fluid dynamics measurement system) and other systems from our product range. The StrainMaster controller, is specifically designed for our Strain measurement systems, and have a simplified interface (meaning that you typically have single connectors with the exception of the connector for the A/D converter). This means that you will have an BNC input labelled 'Trig In' (on the back of the controller
unit) that you should use for the external trigger. Below that input, is an output for a BNC trigger, that can be used to trigger an external device from the Strain system. For further information on the wiring of our StrainMaster controller, please refer to the StrainMaster portable manual (pages 39-40), which is available for download from our website (for free as long as you log in using credentials verified by LaVision - i.e. you need to register using the order/dongle number that you can find in DaVis under 'Help'->'About' or on the installation disk provided with the system) or you can download from here:

https://fta.lavision.de/link/6pO5Gg0zvDLceOUtJSsyez

You can therefore use the TTL input trigger to trigger your recording if you select the 'Trigger image #' option in the 'Recording sequence' tab when in the recording dialog (as explained in the DaVis manual, an extract of which is attached - 'Recording_options'). You can also use an oscilloscope to check the triggers (you can also set up an output trigger from the controller to trigger when the first image is taken, so that you can check the timing and triggering of the system) 

As I mentioned previously, a better option would be to use the integrated A/D converter, and to set up a trigger on a given threshold of the ADC channel value. 
Please note that the integrated ADC of your controller can be used to directly get your input signal and use that as a trigger. You can setup which channels of the ADC you'd like to scan, and the information will be stored with your recorded images. To use the ADC channels to trigger a recording, you should use the same option as before ('Trigger image#') but you will have an additional entry using the ADC. Here you can use any desired threshold on the values recorded by the ADC to start the image acquisition. Please note that you should set up the A/D converter appropriately, as explained in the attached document, by accessing the 'AD Converter' tab under 'Device Settings'. Please note that if the appropriate scaling is also set up in DaVis, your ADC data will be saved accordingly as the value of interest instead of a voltage value (e.g. a load in [N] instead of voltage). Please find the attached 'ADC_device_settings' document with a detailed explanations of these aspects.

Regarding the possibility of setting up more complex recording procedures, you can use the 'Recording sequence' dialog for complex triggering capabilities by defining for instance loops, or scans that can be set up by the user in a complex manner. For more information, please refer to the attached 'Recording_options' 
document.   

I hope that the information herein is useful, and please let me know if I can be of further assistance.

Kind regards,
Your Service-Team
Dr. Alex Nila 
-----