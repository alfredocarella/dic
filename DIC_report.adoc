= Digital Image Correlation setup for fatigue research
:Author:    Alfredo Carella
:Email:     <Alfredo.Carella@hioa.no>
:Date:      2016-03-29
:Revision:  1.0 - Preliminary version
:imagesdir: ./images
:sourcedir: ./sources
:icons: font
:source-highlighter: coderay
:coderay-linenums-mode: table
:stem: latexmath
:toc:
:toclevels: 3
:toc-title: Contents

:sectnums:
include::objective_and_challenges.adoc[leveloffset=+1]

<<<<<
include::trigger_setup.adoc[leveloffset=+1]

<<<<<
include::signal_conditioning.adoc[leveloffset=+1]

<<<<<
include::unanswered_questions.adoc[leveloffset=+1]

:!sectnums:
<<<<<
[[Appendix-A]]
== Appendix A: Arduino sketch

[source,c++,numbered]
-----
include::{sourcedir}/trigger_firmata.ino[]
-----

<<<<<
[[Appendix-B]]
== Appendix B: Python acquisition example

[source,python,linenums]
-----
include::{sourcedir}/serial_com_example.py[]
-----

<<<<<
[[Appendix-C]]
== Appendix C: E-mail exchange with LaVision

include::emails_LaVision.adoc[leveloffset=+1]
